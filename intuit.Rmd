---
title: Intuit Trees and Nets
output: html_document
---

* Team-lead GitLab id:
* Group number:
* Group name:
* Team member names:

```{r r_setup, include = FALSE}
## initial settings
knitr::opts_chunk$set(
  comment = NA,
  echo = TRUE,
  error = TRUE,
  cache = FALSE,
  message = FALSE,
  dpi = 144,
  warning = FALSE
)

## width to use when printing tables etc.
options(
  width = 250,
  scipen = 100,
  max.print = 5000, 
  stringsAsFactors = FALSE
)

## load radiant packages if needed
if (!exists("r_environment")) library(radiant)
```


```{r}
## loading the data. Note that data must be loaded from the data/
## in the rstudio project directory
intuit75k <- readr::read_rds(file.path(radiant.data::find_dropbox(), "MGTA455-2019/data/intuit75k.rds"))
```

Below an example of how you could include another Rmd file inside your main assignment Rmarkdown document. Note:  don't add a YAML header to the _child_ file! 

```{r, child = "model1.Rmd"}
## NN
library(nnet)
library(caret)
library(radiant)
intuit75k <- mutate(intuit75k, zip_va = ifelse(zip == "00801", 1, 0))
intuit75k <- mutate(intuit75k, zip_va2 = ifelse(zip == "00804",1, 0))
intuit75k <- mutate(intuit75k, zip_bins = as.factor(zip_bins))
## some formating options
options(
  width = 250,
  scipen = 100,
  max.print = 5000,
  stringsAsFactors = FALSE
)

training <- intuit75k %>% filter(training == 1)
testing <- intuit75k %>% filter(training == 0)

rvar <- "res1"
evar <- c( "numords", "dollars", "last", "sincepurch", 
    "version1", "owntaxprod", "upgraded","zip_va","zip_bins","zip_va2")
lev <- "Yes"

eval_dat <- tibble::tibble(
  res1 = testing$res1,
  training = testing$training
)


breakeven <- 1.41/60
intuit75k <- mutate(intuit75k, zip_va = ifelse(zip == "00801", 1, 0))
intuit75k <- mutate(intuit75k, zip_va2 = ifelse(zip == "00804",1, 0))
intuit75k <- mutate(intuit75k, zip_bins = as.factor(zip_bins))

training <- intuit75k %>% filter(training == 1)
testing <- intuit75k %>% filter(training == 0)



perf_calc <- function(
  dat,sms,perf = "res1",lev = "Yes",
  nr_cust = 801821 - 38487
){
  perc_sms <- mean(pull(dat,!!sms))
  nr_sms <- sum(pull(dat,!!sms))
  dat <- filter(dat,pull(dat,!!sms))
  mailto_number <- ceiling(nr_cust*perc_sms)
  rep_rate <- mean(pull(dat,!!perf) == lev,na.rm = TRUE)
  expected_buyer <- ceiling(mailto_number *rep_rate/2)
  profit <- 60*expected_buyer - 1.41*mailto_number
  return(profit)
}



result <- nn(
  intuit75k, 
  rvar = "res1", 
  evar = c(
    "numords", "dollars", "last", 
    "version1", "owntaxprod", "upgraded", "zip_bins","zip_va","zip_va2"
  ), 
  lev = "Yes", 
  size = 4, 
  seed = 1234, 
  data_filter = "training==1;"
)
summary(result, prn = TRUE)
pred <- predict(result, pred_data = testing)

testing <-  testing %>% mutate(purch_prob9 = pred$Prediction)

testing  <-testing %>% mutate(mailto_nn9 = purch_prob9/2 > breakeven)

```


```{r}
res_nn2 <- perf_calc(testing, "mailto_nn9",perf = "res1",lev = "Yes",nr_cust = 801821 - 38487) 
res_nn2 <- data.frame(res_nn2)
res_nn2
```

In the Neural Network approach, the expected profit is `r format_nr(res_nn2$profit,"$", dec = 2)` with a ROME of `r format_nr(res_nn2$ROME, perc = TRUE)`.



```{r, message = FALSE}
#Load Libraries
library(tidyverse)
library(radiant.data)
library(radiant)
library(readr)
library(glmnet)
library(caret)
library(pROC)
```

#Profit Function
```{r}
library(dplyr)
perf_calc1 <- function(test_1,a,k,b="training")
{
test <- test_1
nr_deal <- sum(test[[a]] == 1)
dat <- filter(test, test[[a]] == 1)                  # only test who were offered
nr_resp <-  sum(dat$res1 == "Yes")
deal_cost <- 1.41* nr_deal
profit_nt <- 60*nr_resp  - (deal_cost)
ROME <- profit_nt/deal_cost * 100
return(c(k,profit_nt,ROME))
}

Write_results <- function(c) 
{
return (cat("The Realized Profit(post experiment) with ",c[1], " method is",c[2],"USD and the ROME is ",c[3],"%\n"))
}

```


```{r}

intuit75k$zip_bin_VA <- as.factor(ifelse(intuit75k$zip==c("00801"),1,0))
intuit75k$zip_bin_VA2 <- as.factor(ifelse(intuit75k$zip==c("00804"),1,0))
intuit75k$zip_bins <- as.factor(intuit75k$zip_bins)
intuit75k$version1 <- as.factor(intuit75k$version1)

train <- intuit75k %>% filter(intuit75k$training==1)
test  <- intuit75k %>% filter(intuit75k$training==0)

result <- logistic(
  train, 
  rvar = "res1", 
  evar = c(
     "zip_bins","numords", "dollars", "last", "owntaxprod", "upgraded",
     "zip_bin_VA2","zip_bin_VA","version1"
  ), 
  lev = "Yes",
  int = c("version1:numords", "version1:last")
 
)
breakeven_12 <- 1.41/60
summary(result)
pred <- predict(result, pred_data = test, conf_lev = 0.95, se = TRUE)
test <- store(test, pred, name = c("purch_prob","purch_prob_lb", "purch_prob_ub"))

test <- test %>%
  mutate(mailto_logit = purch_prob/2 > breakeven_12,
         mailto_logit_lb = purch_prob_lb/2 > breakeven_12)

for(i in c(0))
{
test <- test%>%
        mutate(mailto_logit1 = ifelse(test$purch_prob/2 > breakeven_12 + i,1,0) ) 
        
Write_results(perf_calc1(test,"mailto_logit1","Logistic"))

} 

confusionMatrix(table(ifelse(test$mailto_logit==TRUE,1,0), ifelse(test$res1=="Yes", 1, 0)))

```

# XGBoost 1

```{r}
a<-intuit75k
train <- a %>% 
          filter(a$training==1) %>% 
         select("res1","zip_bins","numords", "dollars", "last", "owntaxprod", "upgraded",
     "zip_bin_VA2","zip_bin_VA","version1")
test1  <- a %>% 
        filter(a$training==0)%>%
        select("res1","zip_bins","numords", "dollars", "last", "owntaxprod", "upgraded",
     "zip_bin_VA2","zip_bin_VA","version1")

#install.packages("xgboost")
require(xgboost)
train_mat <- Matrix::sparse.model.matrix(res1~.-1, data = train)
res1_mat <- ifelse(train$res1=="Yes",1,0)
test_mat <- Matrix::sparse.model.matrix(res1~.-1, data = test1)

set.seed(1)
#parameters = {'tree_method': 'exact'} 
xgb_mod <- xgboost(data = train_mat, label = res1_mat,
                        eta = 0.1,
                         nround = 100,
                         max.depth = 4,
                         min_child_weight = 10,
                         gamma = 1,
                         colsample_bytree =0.7,
                         subsample = 0.8,
                         cv.fold = 5,
                        # tree_method = hist,
                         objective = "binary:logistic",verbose = 0)
test$pred_xgb1 <- predict(xgb_mod, test_mat)

test <- test %>% mutate(probab_xgb1_pred = pred_xgb1 ) 

test <- test %>% mutate(probab_xgb1_Mail_to = pred_xgb1/2 > breakeven_12 ) 

for(i in c(0))
{
test <- test%>%
        mutate(probab_xgb1 = ifelse(test$pred_xgb1/2 > breakeven_12 + i,1,0) ) 
        
Write_results(perf_calc1(test,"probab_xgb1","Logistic"))

}  
print(mean(pred == ifelse(test$res1=="Yes", 1, 0)))


#test <- test %>% mutate(mailto_gbm1 = probab_gbm1_pred > breakeven_12 ) 
confusionMatrix(table(ifelse(test$probab_xgb1==TRUE,1,0), ifelse(test$res1=="Yes", 1, 0)))
```

# Xgboost 2
```{r}
a<-intuit75k
train <- a %>% 
          filter(a$training==1) %>% 
         select("res1","zip_bins","numords", "dollars", "last", "owntaxprod", "upgraded",
     "zip_bin_VA2","zip_bin_VA","version1")
test1  <- a %>% 
        filter(a$training==0)%>%
        select("res1","zip_bins","numords", "dollars", "last", "owntaxprod", "upgraded",
     "zip_bin_VA2","zip_bin_VA","version1")

#install.packages("xgboost")
require(xgboost)
train_mat <- Matrix::sparse.model.matrix(res1~.-1, data = train)
res1_mat <- ifelse(train$res1=="Yes",1,0)
test_mat <- Matrix::sparse.model.matrix(res1~.-1, data = test1)

set.seed(1)
#parameters = {'tree_method': 'exact'} 
xgb_mod <- xgboost(data = train_mat, label = res1_mat,
                        eta = 0.01,
                         nround = 1030,
                         max.depth = 4,
                         min_child_weight = 10,
                         gamma = 1,
                         colsample_bytree =0.7,
                         subsample = 0.8,
                         cv.fold = 5,
                        # tree_method = hist,
                         objective = "binary:logistic",verbose = 0)
test$pred_xgb <- predict(xgb_mod, test_mat)

test <- test %>% mutate(probab_xgb2_pred = pred_xgb ) 

test <- test %>% mutate(probab_gbm1_Mail_to = pred_xgb/2 > breakeven_12 ) 

for(i in c(0))
{
test <- test%>%
        mutate(probab_gbm1 = ifelse(test$probab_xgb2_pred/2 > breakeven_12 + i,1,0) ) 
        
Write_results(perf_calc1(test,"probab_gbm1","Logistic"))

}  
print(mean(pred == ifelse(test$res1=="Yes", 1, 0)))


#test <- test %>% mutate(mailto_gbm1 = probab_gbm1_pred > breakeven_12 ) 
confusionMatrix(table(ifelse(test$probab_gbm1==TRUE,1,0), ifelse(test$res1=="Yes", 1, 0)))
```
\
#Random Forrest
```{r}
library(ranger)
intuit75k$zip_bin_VA <- as.factor(ifelse(intuit75k$zip==c("00801"),1,0))
intuit75k$zip_bin_VA2 <- as.factor(ifelse(intuit75k$zip==c("00804"),1,0))
intuit75k$zip_bins <- as.factor(intuit75k$zip_bins)
intuit75k$res1 <- as.factor(intuit75k$res1)
a <- intuit75k

train <- a %>% 
          filter(a$training==1) %>% 
         select("res1","zip_bins","numords", "dollars", "last", "owntaxprod", "upgraded",
     "zip_bin_VA2","zip_bin_VA","version1")
train_label <- a %>% 
          filter(a$training==1) %>% 
         select("res1")

test1  <- a %>% 
        filter(a$training==0)%>%
        select("res1","zip_bins","numords", "dollars", "last", "owntaxprod", "upgraded",
     "zip_bin_VA2","zip_bin_VA","version1")



rf3 <- ranger(res1 ~ .- id - training - zip, probability = TRUE,
              data = train, num.trees = 709,
              mtry = 3, respect.unordered.factors = "order", seed = 123,
              sample.fraction = 0.70,
              min.node.size = 250)
              
              
pred_rf <- predict(rf3, test1)
test$pred_rf <- pred_rf$predictions[, "Yes"]

for(i in c(0))
{
test <- test%>%
        mutate(mailto_pred_rf = ifelse(test$pred_rf/2 > 0.0235 + i,1,0) ) 
        
Write_results(perf_calc1(test,"mailto_pred_rf","Logistic"))

}  
#print(mean(pred == ifelse(test$res1=="Yes", 1, 0)))
test <- test %>% mutate(mailto_pred_rf = mailto_pred_rf/2 > 0.0235 ) 
confusionMatrix(table(ifelse(test$mailto_pred_rf==TRUE,1,0), ifelse(test$res1=="Yes", 1, 0)))
```


```{r}

# install.packages("gbm")
#set.seed(1234)
library(gbm)

intuit75k <- readr::read_rds(file.path(radiant.data::find_dropbox(), "MGTA455-2019/data/intuit75k.rds"))
intuit75k <- mutate(intuit75k, zip_va = ifelse(zip == "00801", 1, 0))
intuit75k <- mutate(intuit75k, zip_va2 = ifelse(zip == "00804",1, 0))
intuit75k <- mutate(intuit75k, zip_bins = as.factor(zip_bins))
intuit75k$res1 <- ifelse(intuit75k$res1 == "Yes", 1, 0)

training_2 <- intuit75k %>% filter(training == 1)
testing_2 <- intuit75k %>% filter(training == 0)
n.trees <- 490

result <- gbm(
 res1 ~ numords+ dollars+last+version1+owntaxprod+upgraded+zip_va+zip_bins+zip_va2,
 data = training_2,
 distribution = "bernoulli",
 n.trees = n.trees,
 interaction.depth = 5,
 shrinkage = 0.01,
 keep.data = FALSE
)

## get variable importance
vimp <- summary(result, plotit = FALSE, n.trees = n.trees)
visualize(vimp, type = "bar", xvar = "var", yvar = "rel.inf")


best.iter <- gbm.perf(result, method = "OOB")
print(best.iter)


# Plot relative influence of each variable
par(mfrow = c(1, 2))
summary(result, n.trees = best.iter)  # using estimated best number of trees

print(pretty.gbm.tree(result, i.tree = result$n.trees))


# Predict on the new data using the "best" number of trees; by default,
# predictions will be on the link scale
Yhat <- predict(result, newdata = testing_2, n.trees = best.iter, type = "response")

perf_calc <- function(
 dat,sms,perf = "res1",lev = "Yes",
 nr_cust = 801821 - 38487
){
 perc_sms <- mean(pull(dat,!!sms))
 nr_sms <- sum(pull(dat,!!sms))
 dat <- filter(dat,pull(dat,!!sms))
 mailto_number <- ceiling(nr_cust*perc_sms)
 rep_rate <- mean(pull(dat,!!perf) == lev,na.rm = TRUE)
 expected_buyer <- ceiling(mailto_number *rep_rate/2)
 profit <- 60*expected_buyer - 1.41*mailto_number
 return(profit)
}


test <-  test %>% mutate(purch_prob_g = Yhat)

test  <-test %>% mutate(mailto_gbm = purch_prob_g/2 > (1.41/60))


res_gbm <- perf_calc(test, "mailto_gbm",perf = "res1",lev = "Yes",nr_cust = 801821 - 38487) 
res_gbm

?confusionMatrix
confusionMatrix(table(ifelse(test$mailto_gbm ==TRUE,1,0), ifelse(test$res1=="Yes", 1, 0)))

```


#Ensemble
```{r}
test$probab_xgb1<- as.numeric(test$probab_xgb1)
test$pred_rf <- as.numeric(test$pred_rf)
test$purch_prob <- as.numeric(test$purch_prob)
test$probab_xgb2_pred<- as.numeric(test$probab_xgb2_pred)

test1 <- test %>% 
        group_by(id) %>%
        summarize(prob_ensemble = sum(purch_prob,purch_prob_g,pred_rf)/3)

test$prob_ensemble <- test1$prob_ensemble

for(i in c(0))
{
test <- test%>%
        mutate(prob_ensemble_mail1 = ifelse(test$prob_ensemble > breakeven_12 + i,1,0) ) 
        
Write_results(perf_calc1(test,"prob_ensemble_mail1","Logistic"))

}  

test <- test %>% mutate(prob_ensemble_mail = prob_ensemble/2 > breakeven_12 ) 

confusionMatrix(table(ifelse(test$prob_ensemble_mail ==TRUE,1,0), ifelse(test$res1=="Yes", 1, 0)))
```


# Scaling Up

```{r}

perf_calc <- function(
  dat,sms,perf = "res1",lev = "Yes",
  nr_cust = 801821 - 38487
)
  {
  perc_sms <- mean(pull(dat,!!sms))
  nr_sms <- sum(pull(dat,!!sms))
  dat <- filter(dat,pull(dat,!!sms))
  mailto_number <- nr_cust*perc_sms
  rep_rate <- sum(pull(dat,!!perf) == lev)/nr_sms
  expected_buyer <- mailto_number * (rep_rate/2)
  profit <- 60*expected_buyer - 1.41*mailto_number
  return(cat(perc_sms,nr_sms,mailto_number,rep_rate,expected_buyer,profit))
}

cat(perf_calc(test,"mailto_logit"),"\n")
cat(perf_calc(test,"probab_xgb1_Mail_to"),"\n")
cat(perf_calc(test,"mailto_gbm"),"\n")
cat(perf_calc(test,"probab_gbm1_Mail_to"),"\n")
cat(perf_calc(test,"mailto_pred_rf"),"\n")
cat(perf_calc(test,"prob_ensemble_mail"))
```
```{r}

# generate mailto_wave 2 column

saveRDS(test%>% select(id,prob_ensemble_mail),"Robbie_Jingyi_Krishn_Xuan_Group2.rds")


```



